package ru.rustore.unrealsdk.payclient.wrappers;

import ru.rustore.sdk.pay.model.PurchaseAvailabilityResult;
import ru.rustore.unitysdk.core.IRuStoreListener;
import ru.rustore.unrealsdk.payclient.callbacks.PurchaseAvailabilityListener;

public class PurchaseAvailabilityListenerWrapper implements IRuStoreListener, PurchaseAvailabilityListener
{
    private Object mutex = new Object();
    private long cppPointer = 0;

    private native void NativeOnFailure(long pointer, Throwable throwable);
    private native void NativeOnSuccess(long pointer, PurchaseAvailabilityResult response);

    public PurchaseAvailabilityListenerWrapper(long cppPointer) {
        this.cppPointer = cppPointer;
    }

    @Override
    public void OnFailure(Throwable throwable) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnFailure(cppPointer, throwable);
            }
        }
    }

    @Override
    public void OnSuccess(PurchaseAvailabilityResult response) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnSuccess(cppPointer, response);
            }
        }
    }

    public void DisposeCppPointer() {
        synchronized (mutex) {
            cppPointer = 0;
        }
    }
}
