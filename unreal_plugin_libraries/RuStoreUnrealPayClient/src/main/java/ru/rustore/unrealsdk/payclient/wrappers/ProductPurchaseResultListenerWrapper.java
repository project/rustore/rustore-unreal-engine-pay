package ru.rustore.unrealsdk.payclient.wrappers;

import ru.rustore.unitysdk.core.IRuStoreListener;
import ru.rustore.unrealsdk.payclient.callbacks.ProductPurchaseResultListener;

public class ProductPurchaseResultListenerWrapper implements IRuStoreListener, ProductPurchaseResultListener
{
    private Object mutex = new Object();
    private long cppPointer = 0;

    private native void NativeOnFailure(long pointer, Throwable throwable);
    private native void NativeOnSuccess(long pointer, String response);

    public ProductPurchaseResultListenerWrapper(long cppPointer) {
        this.cppPointer = cppPointer;
    }

    @Override
    public void OnFailure(Throwable throwable) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnFailure(cppPointer, throwable);
            }
        }
    }

    @Override
    public void OnSuccess(String response) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnSuccess(cppPointer, response);
            }
        }
    }

    public void DisposeCppPointer() {
        synchronized (mutex) {
            cppPointer = 0;
        }
    }
}
