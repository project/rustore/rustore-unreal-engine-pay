package ru.rustore.unrealsdk.payclient.callbacks;

import ru.rustore.sdk.pay.model.PurchaseAvailabilityResult;

public interface PurchaseAvailabilityListener {

    void OnFailure(Throwable throwable);
    void OnSuccess(PurchaseAvailabilityResult response);
}
