package ru.rustore.unrealsdk.payclient.callbacks;

public interface ConfirmTwoStepPurchaseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess();
}
