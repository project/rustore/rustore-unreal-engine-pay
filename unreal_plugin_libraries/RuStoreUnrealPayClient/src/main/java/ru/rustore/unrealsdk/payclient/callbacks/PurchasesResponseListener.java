package ru.rustore.unrealsdk.payclient.callbacks;

public interface PurchasesResponseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(String response);
}
