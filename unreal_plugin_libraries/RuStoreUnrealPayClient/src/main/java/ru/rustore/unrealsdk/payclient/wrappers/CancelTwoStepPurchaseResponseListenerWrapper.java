package ru.rustore.unrealsdk.payclient.wrappers;

import ru.rustore.unrealsdk.payclient.callbacks.CancelTwoStepPurchaseListener;
import ru.rustore.unitysdk.core.IRuStoreListener;

public class CancelTwoStepPurchaseResponseListenerWrapper implements IRuStoreListener, CancelTwoStepPurchaseListener
{
    private Object mutex = new Object();
    private long cppPointer = 0;

    private native void NativeOnFailure(long pointer, Throwable throwable);
    private native void NativeOnSuccess(long pointer);

    public CancelTwoStepPurchaseResponseListenerWrapper(long cppPointer) {
        this.cppPointer = cppPointer;
    }

    @Override
    public void OnFailure(Throwable throwable) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnFailure(cppPointer, throwable);
            }
        }
    }

    @Override
    public void OnSuccess() {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnSuccess(cppPointer);
            }
        }
    }

    public void DisposeCppPointer() {
        synchronized (mutex) {
            cppPointer = 0;
        }
    }
}
