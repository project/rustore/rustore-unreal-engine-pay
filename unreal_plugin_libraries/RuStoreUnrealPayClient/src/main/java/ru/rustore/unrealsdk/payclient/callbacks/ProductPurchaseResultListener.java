package ru.rustore.unrealsdk.payclient.callbacks;

public interface ProductPurchaseResultListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(String response);
}
