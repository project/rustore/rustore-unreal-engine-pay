package ru.rustore.unrealsdk.payclient.callbacks;

public interface ProductsResponseListener {
    public void OnFailure(Throwable throwable);
    public void OnSuccess(String response);
}
