package ru.rustore.unrealsdk.payclient.callbacks;

public interface PurchaseResponseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(String response);
}
