package ru.rustore.unrealsdk.payclient

import android.net.Uri
import com.google.gson.GsonBuilder
import ru.rustore.sdk.core.util.RuStoreUtils
import ru.rustore.sdk.pay.RuStorePayClient
import ru.rustore.sdk.pay.model.DeveloperPayload
import ru.rustore.sdk.pay.model.OrderId
import ru.rustore.sdk.pay.model.ProductId
import ru.rustore.sdk.pay.model.ProductPurchaseParams
import ru.rustore.sdk.pay.model.PurchaseId
import ru.rustore.sdk.pay.model.Quantity
import ru.rustore.unitysdk.core.PlayerProvider
import ru.rustore.unrealsdk.payclient.callbacks.CancelTwoStepPurchaseListener
import ru.rustore.unrealsdk.payclient.callbacks.ConfirmTwoStepPurchaseListener
import ru.rustore.unrealsdk.payclient.callbacks.ProductPurchaseResultListener
import ru.rustore.unrealsdk.payclient.callbacks.ProductsResponseListener
import ru.rustore.unrealsdk.payclient.callbacks.PurchaseAvailabilityListener
import ru.rustore.unrealsdk.payclient.callbacks.PurchaseResponseListener
import ru.rustore.unrealsdk.payclient.callbacks.PurchasesResponseListener

object RuStoreUnrealPayClient {

	private val gson = GsonBuilder()
		.registerTypeAdapter(Uri::class.java, UriTypeAdapter())
		.create()

	fun getPurchaseAvailability(listener: PurchaseAvailabilityListener) {
		RuStorePayClient.instance.getPurchaseInteractor().getPurchaseAvailability()
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun isRuStoreInstalled(): Boolean =
		PlayerProvider.getCurrentActivity()?.application?.let {
			return RuStoreUtils.isRuStoreInstalled(it)
		} ?: false

	fun getProducts(productIds: Array<String>, listener: ProductsResponseListener) {
		val ids = productIds.map { ProductId(it) }.toList()
		RuStorePayClient.instance.getProductInteractor().getProducts(ids)
			.addOnSuccessListener { result -> listener.OnSuccess(gson.toJson(result)) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun getPurchases(listener: PurchasesResponseListener) {
		RuStorePayClient.instance.getPurchaseInteractor().getPurchases()
			.addOnSuccessListener { result -> listener.OnSuccess(gson.toJson(result)) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun getPurchase(purchaseId: String, listener: PurchaseResponseListener) {
		RuStorePayClient.instance.getPurchaseInteractor().getPurchase(PurchaseId(purchaseId))
			.addOnSuccessListener { result -> listener.OnSuccess(gson.toJson(result)) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun purchaseOneStep(productId: String, developerPayload: String?, orderId: String?, quantity: Int, listener: ProductPurchaseResultListener) {
		RuStorePayClient.instance.getPurchaseInteractor().purchaseOneStep(
			ProductPurchaseParams(
				productId = ProductId(productId),
				orderId = orderId?.let{ OrderId(it) },
				quantity = Quantity(quantity),
				developerPayload = developerPayload?.let { DeveloperPayload(it) }
			))
			.addOnSuccessListener { result ->
				val json = """{"type":"${result.javaClass.simpleName}","data":${gson.toJson(result)}}""".trimIndent()
				listener.OnSuccess(json)
			}
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun purchaseTwoStep(productId: String, developerPayload: String?, orderId: String?, quantity: Int, listener: ProductPurchaseResultListener) {
		RuStorePayClient.instance.getPurchaseInteractor().purchaseTwoStep(
			ProductPurchaseParams(
				productId = ProductId(productId),
				orderId = orderId?.let{ OrderId(it) },
				quantity = Quantity(quantity),
				developerPayload = developerPayload?.let { DeveloperPayload(it) }
			))
			.addOnSuccessListener { result ->
				val json = """{"type":"${result.javaClass.simpleName}","data":${gson.toJson(result)}}""".trimIndent()
				listener.OnSuccess(json)
			}
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun confirmTwoStepPurchase(purchaseId: String, developerPayload: String?, listener: ConfirmTwoStepPurchaseListener) {
		RuStorePayClient.instance.getPurchaseInteractor().confirmTwoStepPurchase(
			purchaseId = PurchaseId(purchaseId),
			developerPayload = developerPayload?.let { DeveloperPayload(it) }
		)
			.addOnSuccessListener { listener.OnSuccess() }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun cancelTwoStepPurchase(purchaseId: String, listener: CancelTwoStepPurchaseListener) {
		RuStorePayClient.instance.getPurchaseInteractor().confirmTwoStepPurchase(
			purchaseId = PurchaseId(purchaseId)
		)
			.addOnSuccessListener { listener.OnSuccess() }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}
}
