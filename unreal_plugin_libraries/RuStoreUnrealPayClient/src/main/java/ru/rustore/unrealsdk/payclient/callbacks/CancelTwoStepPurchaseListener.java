package ru.rustore.unrealsdk.payclient.callbacks;

public interface CancelTwoStepPurchaseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess();
}
