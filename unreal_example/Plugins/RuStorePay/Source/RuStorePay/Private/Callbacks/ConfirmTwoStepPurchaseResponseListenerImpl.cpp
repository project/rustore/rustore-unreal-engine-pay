// Copyright Epic Games, Inc. All Rights Reserved.

#include "ConfirmTwoStepPurchaseResponseListenerImpl.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_ConfirmTwoStepPurchaseResponseListenerWrapper_NativeOnFailure(JNIEnv*, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<ConfirmTwoStepPurchaseResponseListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_ConfirmTwoStepPurchaseResponseListenerWrapper_NativeOnSuccess(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<ConfirmTwoStepPurchaseResponseListenerImpl*>(pointer);
        castobj->OnSuccess();
    }
}
#endif
