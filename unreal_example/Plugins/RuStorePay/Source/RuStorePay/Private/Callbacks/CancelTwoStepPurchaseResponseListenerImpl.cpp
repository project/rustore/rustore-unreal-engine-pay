// Copyright Epic Games, Inc. All Rights Reserved.

#include "CancelTwoStepPurchaseResponseListenerImpl.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_CancelTwoStepPurchaseResponseListenerWrapper_NativeOnFailure(JNIEnv*, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<CancelTwoStepPurchaseResponseListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_CancelTwoStepPurchaseResponseListenerWrapper_NativeOnSuccess(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<CancelTwoStepPurchaseResponseListenerImpl*>(pointer);
        castobj->OnSuccess();
    }
}
#endif
