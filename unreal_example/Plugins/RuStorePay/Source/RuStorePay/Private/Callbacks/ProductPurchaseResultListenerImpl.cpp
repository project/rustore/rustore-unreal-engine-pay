// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProductPurchaseResultListenerImpl.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

//FURuStorePayProductPurchaseResult* ProductPurchaseResultListenerImpl::ConvertResponse(AndroidJavaObject* responseObject)
//{
//    FString resultType = "";
//
//    AndroidJavaObject* javaClass = responseObject->CallAJClass("getClass");
//
//    FString str = javaClass->CallFString("getName");
//    TArray<FString> _className;
//    str.ParseIntoArray(_className, TEXT("$"), true);
//    if (_className.Num() > 0) resultType = _className.Last();
//
//    delete javaClass;
//
//    if (resultType == "SuccessProductPurchaseResult")
//    {
//        auto result = new FURuStorePaySuccessProductPurchaseResult();
//
//        result->orderId = responseObject->GetFString("orderId");
//        result->purchaseId = responseObject->GetFString("purchaseId");
//        result->productId = responseObject->GetFString("productId");
//        result->invoiceId = responseObject->GetFString("invoiceId");
//        result->subscriptionToken = responseObject->GetFString("subscriptionToken");
//        result->sandbox = responseObject->GetBool("sandbox");
//
//        return result;
//    }
//    else if (resultType == "CancelledProductPurchaseResult")
//    {
//        auto result = new FURuStorePayCancelledProductPurchaseResult();
//        result->purchaseId = responseObject->GetFString("purchaseId");
//        result->sandbox = responseObject->GetBool("sandbox");
//
//        return result;
//    }
//    else if (resultType == "FailureProductPurchaseResult")
//    {
//        auto result = new FURuStorePayFailureProductPurchaseResult();
//
//        result->purchaseId = responseObject->GetFString("purchaseId");
//        result->invoiceId = responseObject->GetFString("invoiceId");
//        result->orderId = responseObject->GetFString("orderId");
//        result->sandbox = responseObject->GetBool("sandbox");
//
//        auto jQuantity = responseObject->GetAJObject("quantity", "Ljava/lang/Integer;");
//        if (jQuantity != nullptr)
//        {
//            result->quantity = jQuantity->CallInt("intValue");
//            delete jQuantity;
//        }
//
//        result->productId = responseObject->GetFString("productId");
//
//        auto jErrorCode = responseObject->GetAJObject("errorCode", "Ljava/lang/Integer;");
//        if (jErrorCode != nullptr)
//        {
//            result->errorCode = jErrorCode->CallInt("intValue");
//            delete jErrorCode;
//        }
//
//        return result;
//    }
//
//    return nullptr;
//}

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_ProductPurchaseResultListenerWrapper_NativeOnFailure(JNIEnv*, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<ProductPurchaseResultListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_ProductPurchaseResultListenerWrapper_NativeOnSuccess(JNIEnv* env, jobject, jlong pointer, jstring result)
    {
        auto castobj = reinterpret_cast<ProductPurchaseResultListenerImpl*>(pointer);
        castobj->OnSuccess(JavaTypeConverter::Convert(env, result));
    }
}
#endif
