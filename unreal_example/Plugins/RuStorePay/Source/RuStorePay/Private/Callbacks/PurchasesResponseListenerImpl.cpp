// Copyright Epic Games, Inc. All Rights Reserved.

#include "PurchasesResponseListenerImpl.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_PurchasesResponseListenerWrapper_NativeOnFailure(JNIEnv*, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<PurchasesResponseListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_PurchasesResponseListenerWrapper_NativeOnSuccess(JNIEnv* env, jobject, jlong pointer, jstring result)
    {
        auto castobj = reinterpret_cast<PurchasesResponseListenerImpl*>(pointer);
        castobj->OnSuccess(JavaTypeConverter::Convert(env, result));
    }
}
#endif
