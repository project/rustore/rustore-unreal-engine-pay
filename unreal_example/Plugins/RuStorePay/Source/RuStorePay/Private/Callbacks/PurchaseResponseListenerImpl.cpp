// Copyright Epic Games, Inc. All Rights Reserved.

#include "PurchaseResponseListenerImpl.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_PurchaseResponseListenerWrapper_NativeOnFailure(JNIEnv*, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<PurchaseResponseListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unrealsdk_payclient_wrappers_PurchaseResponseListenerWrapper_NativeOnSuccess(JNIEnv* env, jobject, jlong pointer, jstring result)
    {
        auto castobj = reinterpret_cast<PurchaseResponseListenerImpl*>(pointer);
        castobj->OnSuccess(JavaTypeConverter::Convert(env, result));
    }
}
#endif
