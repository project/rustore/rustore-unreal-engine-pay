// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayClient.h"

#include "CancelTwoStepPurchaseResponseListenerImpl.h"
#include "ConfirmTwoStepPurchaseResponseListenerImpl.h"
#include "FURuStorePayCancelledProductPurchaseResult.h"
#include "FURuStorePayFailureProductPurchaseResult.h"
#include "FURuStorePaySuccessProductPurchaseResult.h"
#include "ProductPurchaseResultListenerImpl.h"
#include "ProductsResponseListenerImpl.h"
#include "PurchaseAvailabilityListenerImpl.h"
#include "PurchaseResponseListenerImpl.h"
#include "PurchasesResponseListenerImpl.h"
#include "URuStoreCore.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

const FString URuStorePayClient::PluginVersion = "7.0.0";
URuStorePayClient* URuStorePayClient::_instance = nullptr;
bool URuStorePayClient::_bIsInstanceInitialized = false;

bool URuStorePayClient::GetIsInitialized()
{
    return bIsInitialized;
}

URuStorePayClient* URuStorePayClient::Instance()
{
    if (!_bIsInstanceInitialized)
    {
        _bIsInstanceInitialized = true;
        _instance = NewObject<URuStorePayClient>(GetTransientPackage());
    }

    return _instance;
}

bool URuStorePayClient::Init()
{
    if (!URuStoreCore::IsPlatformSupported()) return false;
    if (bIsInitialized) return false;

    _instance->AddToRoot();

    URuStoreCore::Instance()->Init();

    auto clientJavaClass = MakeShared<AndroidJavaClass>("ru/rustore/unrealsdk/payclient/RuStoreUnrealPayClient");
    _clientWrapper = clientJavaClass->GetStaticAJObject("INSTANCE");
    
    return bIsInitialized = true;
}

void URuStorePayClient::Dispose()
{
    if (bIsInitialized)
    {
        bIsInitialized = false;
        ListenerRemoveAll();
        delete _clientWrapper;
        _instance->RemoveFromRoot();
    }
}

void URuStorePayClient::ConditionalBeginDestroy()
{
    Super::ConditionalBeginDestroy();

    Dispose();
    if (_bIsInstanceInitialized) _bIsInstanceInitialized = false;
}

bool URuStorePayClient::IsRuStoreInstalled()
{
    if (!URuStoreCore::IsPlatformSupported()) return false;
    if (!bIsInitialized) return false;

    return _clientWrapper->CallBool(TEXT("isRuStoreInstalled"));
}

URuStorePayProductId* URuStorePayClient::MakeProductId(FString value)
{
    auto productId = NewObject<URuStorePayProductId>(GetTransientPackage());
    productId->value = value;

    return productId;
}

TArray<URuStorePayProductId*> URuStorePayClient::MakeProductIdArray(TArray<FString> productIds)
{
    TArray<URuStorePayProductId*> _productIds;
    for (const auto item : productIds)
    {
        auto productId = NewObject<URuStorePayProductId>(GetTransientPackage());
        productId->value = item;
        _productIds.Add(productId);
    }

    return _productIds;
}

URuStorePayOrderId* URuStorePayClient::MakeOrderId(FString value)
{
    auto orderId = NewObject<URuStorePayOrderId>(GetTransientPackage());
    orderId->value = value;

    return orderId;
}

URuStorePayQuantity* URuStorePayClient::MakeQuantity(int value)
{
    auto quantity = NewObject<URuStorePayQuantity>(GetTransientPackage());
    quantity->value = value;

    return quantity;
}

URuStorePayDeveloperPayload* URuStorePayClient::MakeDeveloperPayload(FString value)
{
    auto orderId = NewObject<URuStorePayDeveloperPayload>(GetTransientPackage());
    orderId->value = value;

    return orderId;
}

FURuStorePayProductPurchaseParams URuStorePayClient::MakeProductPurchaseParams(URuStorePayProductId* productId, URuStorePayDeveloperPayload* developerPayload, URuStorePayOrderId* orderId, URuStorePayQuantity* quantity)
{
    FURuStorePayProductPurchaseParams params;

    return params;
}


long URuStorePayClient::GetPurchaseAvailability(TFunction<void(long, TSharedPtr<FURuStorePayPurchaseAvailabilityResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto listener = ListenerBind(new PurchaseAvailabilityListenerImpl(onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid(TEXT("getPurchaseAvailability"), listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::GetProducts(TArray<URuStorePayProductId*>& productIds, TFunction<void(long, TSharedPtr<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto _onSuccess = [onSuccess](long requestId, FString response) {
        auto products = DataConverter::ParseProducts(response);
        onSuccess(requestId, products);
    };

    TArray<FString> _productIds;
    for (const auto item : productIds)
    {
        if (item != nullptr) _productIds.Add(item->value);
    }

    auto listener = ListenerBind(new ProductsResponseListenerImpl(_onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid(TEXT("getProducts"), _productIds, listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::GetPurchases(TFunction<void(long, TSharedPtr<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto _onSuccess = [onSuccess](long requestId, FString response) {
        auto purchases = DataConverter::ParsePurchases(response);
        onSuccess(requestId, purchases);
    };

    auto listener = ListenerBind(new PurchasesResponseListenerImpl(_onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid(TEXT("getPurchases"), listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::GetPurchase(URuStorePayPurchaseId* purchaseId, TFunction<void(long, TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto _onSuccess = [onSuccess](long requestId, FString response) {
        auto purchase = DataConverter::ParsePurchase(response);
        onSuccess(requestId, purchase);
    };

    auto listener = ListenerBind(new PurchaseResponseListenerImpl(_onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("getPurchase", purchaseId->value, listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::PurchaseOneStep(FURuStorePayProductPurchaseParams& productPurchaseParams, TFunction<void(long, TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto _onSuccess = [onSuccess](long requestId, FString response) {
        auto result = DataConverter::ParseProductPurchase(response);
        onSuccess(requestId, result);
    };

    FString _developerPayload = "";
    if (productPurchaseParams.developerPayload != nullptr) _developerPayload = productPurchaseParams.developerPayload->value;

    FString _orderId = "";
    if (productPurchaseParams.orderId != nullptr) _orderId = productPurchaseParams.orderId->value;

    int _quantity = 1;
    if (productPurchaseParams.quantity != nullptr) _quantity = productPurchaseParams.quantity->value;

    auto listener = ListenerBind(new ProductPurchaseResultListenerImpl(_onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("purchaseOneStep", productPurchaseParams.productId->value, _developerPayload, _orderId, _quantity, listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::PurchaseTwoStep(FURuStorePayProductPurchaseParams& productPurchaseParams, TFunction<void(long, TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto _onSuccess = [onSuccess](long requestId, FString response) {
        auto result = DataConverter::ParseProductPurchase(response);
        onSuccess(requestId, result);
    };

    FString _developerPayload = "";
    if (productPurchaseParams.developerPayload != nullptr) _developerPayload = productPurchaseParams.developerPayload->value;

    FString _orderId = "";
    if (productPurchaseParams.orderId != nullptr) _orderId = productPurchaseParams.orderId->value;

    int _quantity = 1;
    if (productPurchaseParams.quantity != nullptr) _quantity = productPurchaseParams.quantity->value;

    auto listener = ListenerBind(new ProductPurchaseResultListenerImpl(_onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("purchaseTwoStep", productPurchaseParams.productId->value, _developerPayload, _orderId, _quantity, listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::ConfirmTwoStepPurchase(URuStorePayPurchaseId* purchaseId, URuStorePayDeveloperPayload* developerPayload, TFunction<void(long)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    FString _developerPayload = "";
    if (developerPayload != nullptr) _developerPayload = developerPayload->value;

    auto listener = ListenerBind(new ConfirmTwoStepPurchaseResponseListenerImpl(onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("confirmTwoStepPurchase", purchaseId->value, _developerPayload, listener->GetJWrapper());

    return listener->GetId();
}

long URuStorePayClient::CancelTwoStepPurchase(URuStorePayPurchaseId* purchaseId, TFunction<void(long)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure)) return 0;
    if (!bIsInitialized) return 0;

    auto listener = ListenerBind(new CancelTwoStepPurchaseResponseListenerImpl(onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("cancelTwoStepPurchase", purchaseId->value, listener->GetJWrapper());

    return listener->GetId();
}
