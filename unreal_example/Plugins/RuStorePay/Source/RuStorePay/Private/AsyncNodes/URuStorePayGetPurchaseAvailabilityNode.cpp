// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayGetPurchaseAvailabilityNode.h"

using namespace RuStoreSDK;

URuStorePayGetPurchaseAvailabilityNode::URuStorePayGetPurchaseAvailabilityNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayGetPurchaseAvailabilityNode* URuStorePayGetPurchaseAvailabilityNode::GetPurchaseAvailabilityAsync(URuStorePayClient* target)
{
    auto node = NewObject<URuStorePayGetPurchaseAvailabilityNode>(GetTransientPackage());
    
    target->GetPurchaseAvailability(
        [node](long requestId, TSharedPtr<FURuStorePayPurchaseAvailabilityResult, ESPMode::ThreadSafe> response) {
            node->Success.Broadcast(*response, FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(FURuStorePayPurchaseAvailabilityResult(), *error);
        }
    );

    return node;
}
