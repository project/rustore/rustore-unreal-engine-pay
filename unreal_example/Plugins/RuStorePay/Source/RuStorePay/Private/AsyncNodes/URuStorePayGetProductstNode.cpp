// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayGetProductsNode.h"

using namespace RuStoreSDK;

URuStorePayGetProductsNode::URuStorePayGetProductsNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayGetProductsNode* URuStorePayGetProductsNode::GetProductsAsync(URuStorePayClient* target, TArray<URuStorePayProductId*> productIds)
{
    auto node = NewObject<URuStorePayGetProductsNode>(GetTransientPackage());
    
    target->GetProducts(
        productIds,
        [node](long requestId, TSharedPtr<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe> response) {
            node->Success.Broadcast(*response, FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(TArray<FURuStorePayProduct>(), *error);
        }
    );

    return node;
}
