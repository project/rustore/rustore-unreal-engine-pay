// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayPurchaseOneStepNode.h"

using namespace RuStoreSDK;

URuStorePayPurchaseOneStepNode::URuStorePayPurchaseOneStepNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayPurchaseOneStepNode* URuStorePayPurchaseOneStepNode::PurchaseOneStepAsync(URuStorePayClient* target, FURuStorePayProductPurchaseParams productPurchaseParams)
{
    auto node = NewObject<URuStorePayPurchaseOneStepNode>(GetTransientPackage());
    
    target->PurchaseOneStep(
        productPurchaseParams,
        [target, node](long requestId, TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe> response) {
            FURuStorePaySuccessProductPurchaseResult success;
            FURuStorePayCancelledProductPurchaseResult cancelled;
            FURuStorePayFailureProductPurchaseResult failure;
            FURuStoreError error;

            auto type = response->GetTypeName();
            if (type.Equals("FURuStorePaySuccessProductPurchaseResult"))
            {
                auto _success = *StaticCastSharedPtr<FURuStorePaySuccessProductPurchaseResult>(response);
                node->Success.Broadcast(_success, cancelled, failure, error);
            }
            else
                if (type.Equals("FURuStorePayCancelledProductPurchaseResult"))
                {
                    auto _cancelled = *StaticCastSharedPtr<FURuStorePayCancelledProductPurchaseResult>(response);
                    node->Cancelled.Broadcast(success, _cancelled, failure, error);
                }
                else
                    if (type.Equals("FURuStorePayFailureProductPurchaseResult"))
                    {
                        auto _failure = *StaticCastSharedPtr<FURuStorePayFailureProductPurchaseResult>(response);
                        node->Failure.Broadcast(success, cancelled, _failure, error);
                    }
                    else
                    {
                        error.name = "FURuStorePayProductPurchaseResult";
                        error.description = "Invalid state";
                        node->Error.Broadcast(success, cancelled, failure, error);
                    }
        },
        [target, node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Error.Broadcast(
                FURuStorePaySuccessProductPurchaseResult(),
                FURuStorePayCancelledProductPurchaseResult(),
                FURuStorePayFailureProductPurchaseResult(),
                *error
            );
        }
    );

    return node;
}
