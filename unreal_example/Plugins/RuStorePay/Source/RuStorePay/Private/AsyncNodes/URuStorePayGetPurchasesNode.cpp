// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayGetPurchasesNode.h"

using namespace RuStoreSDK;

URuStorePayGetPurchasesNode::URuStorePayGetPurchasesNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayGetPurchasesNode* URuStorePayGetPurchasesNode::GetPurchasesAsync(URuStorePayClient* target)
{
    auto node = NewObject<URuStorePayGetPurchasesNode>(GetTransientPackage());

    target->GetPurchases(
        [node](long requestId, TSharedPtr<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe> response) {
            node->Success.Broadcast(*response, FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(TArray<FURuStorePayPurchase>(), *error);
        }
    );

    return node;
}
