// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayConfirmTwoStepPurchaseNode.h"

using namespace RuStoreSDK;

URuStorePayConfirmTwoStepPurchaseNode::URuStorePayConfirmTwoStepPurchaseNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayConfirmTwoStepPurchaseNode* URuStorePayConfirmTwoStepPurchaseNode::ConfirmTwoStepPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId, URuStorePayDeveloperPayload* developerPayload)
{
    auto node = NewObject<URuStorePayConfirmTwoStepPurchaseNode>(GetTransientPackage());

    target->ConfirmTwoStepPurchase(
        purchaseId,
        developerPayload,
        [node](long requestId) {
            node->Success.Broadcast(FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(*error);
        }
    );

    return node;
}
