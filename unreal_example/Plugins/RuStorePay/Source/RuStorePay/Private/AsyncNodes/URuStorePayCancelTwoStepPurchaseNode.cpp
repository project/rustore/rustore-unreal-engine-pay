// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayCancelTwoStepPurchaseNode.h"

using namespace RuStoreSDK;

URuStorePayCancelTwoStepPurchaseNode::URuStorePayCancelTwoStepPurchaseNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayCancelTwoStepPurchaseNode* URuStorePayCancelTwoStepPurchaseNode::CancelTwoStepPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId)
{
    auto node = NewObject<URuStorePayCancelTwoStepPurchaseNode>(GetTransientPackage());

    target->CancelTwoStepPurchase(
        purchaseId,
        [node](long requestId) {
            node->Success.Broadcast(FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(*error);
        }
    );

    return node;
}
