// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStorePayGetPurchaseNode.h"

using namespace RuStoreSDK;

URuStorePayGetPurchaseNode::URuStorePayGetPurchaseNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

URuStorePayGetPurchaseNode* URuStorePayGetPurchaseNode::GetPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId)
{
    auto node = NewObject<URuStorePayGetPurchaseNode>(GetTransientPackage());

    target->GetPurchase(
        purchaseId,
        [node](long requestId, TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe> response) {
            node->Success.Broadcast(*response, FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(FURuStorePayPurchase(), *error);
        }
    );

    return node;
}
