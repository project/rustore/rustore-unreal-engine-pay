// Copyright Epic Games, Inc. All Rights Reserved.

#include "DataConverter.h"
#include "FURuStorePayCancelledProductPurchaseResult.h"
#include "FURuStorePayFailureProductPurchaseResult.h"
#include "FURuStorePaySuccessProductPurchaseResult.h"

using namespace RuStoreSDK;
using namespace RuStoreSDK::Pay;

const TSharedRef<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe> DataConverter::ParseProducts(const FString& json)
{
    const auto products = MakeShared<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe>();

    TArray<TSharedPtr<FJsonValue>> jsonProducts;
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(json);
    if (!FJsonSerializer::Deserialize(reader, jsonProducts)) return products;
    
    for (const auto& item : jsonProducts)
    {
        TSharedPtr<FJsonObject> jsonProduct = item->AsObject();
        if (jsonProduct.IsValid())
        {
            FURuStorePayProduct product;
            
            product.amountLabel->value = jsonProduct->GetObjectField("amountLabel")->GetStringField("value");
            product.currency->value = jsonProduct->GetObjectField("currency")->GetStringField("value");
            if (auto field = jsonProduct->GetObjectField("description"))
            {
                product.description = NewObject<URuStorePayDescription>(GetTransientPackage());
                product.description->value = field->GetStringField("value");
            }
            product.imageUrl->value = jsonProduct->GetObjectField("imageUrl")->GetStringField("value");
            if (auto field = jsonProduct->GetObjectField("price"))
            {
                product.price = NewObject<URuStorePayPrice>(GetTransientPackage());
                product.price->value = field->GetIntegerField("value");
            }
            product.productId->value = jsonProduct->GetObjectField("productId")->GetStringField("value");
            if (auto field = jsonProduct->GetObjectField("promoImageUrl"))
            {
                product.promoImageUrl = NewObject<URuStorePayUrl>(GetTransientPackage());
                product.promoImageUrl->value = field->GetStringField("value");
            }
            product.title->value = jsonProduct->GetObjectField("title")->GetStringField("value");
            product.type = GetEnumByNameString<EURuStorePayProductType>(jsonProduct->GetStringField("type"));
            
            products->Add(product);
        }
    }

    return products;
}

const TSharedRef<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe> DataConverter::ParsePurchases(const FString& json)
{
    const auto purchases = MakeShared<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe>();

    TArray<TSharedPtr<FJsonValue>> jsonPurchases;
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(json);
    if (!FJsonSerializer::Deserialize(reader, jsonPurchases)) return purchases;
    
    for (const auto& item : jsonPurchases)
    {
        if (!item.IsValid()) continue;
        
        auto jsonPurchase = item->AsObject();
        if (!jsonPurchase.IsValid()) continue;
        
        auto purchase = ParsePurchase(jsonPurchase);
        if (!purchase.IsValid()) continue;

        purchases->Add(*purchase);
    }
    
    return purchases;
}

const TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe> DataConverter::ParsePurchase(const FString& json)
{
    TSharedPtr<FJsonObject> jsonPurchase;
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(json);
    
    return FJsonSerializer::Deserialize(reader, jsonPurchase)
        ? ParsePurchase(jsonPurchase)
        : nullptr;
}

const TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe> DataConverter::ParsePurchase(TSharedPtr<FJsonObject> jsonPurchase)
{
    if (!jsonPurchase.IsValid()) return nullptr;

    const auto purchase = MakeShared<FURuStorePayPurchase, ESPMode::ThreadSafe>();

    purchase->amountLabel->value = jsonPurchase->GetObjectField("amountLabel")->GetStringField("value");
    purchase->currency->value = jsonPurchase->GetObjectField("currency")->GetStringField("value");
    purchase->description->value = jsonPurchase->GetObjectField("description")->GetStringField("value");
    if (auto field = jsonPurchase->GetObjectField("developerPayload"))
    {
        purchase->developerPayload = NewObject<URuStorePayDeveloperPayload>(GetTransientPackage());
        purchase->developerPayload->value = field->GetStringField("value");
    }
    purchase->invoiceId->value = jsonPurchase->GetObjectField("invoiceId")->GetStringField("value");
    if (auto field = jsonPurchase->GetObjectField("orderId"))
    {
        purchase->orderId = NewObject<URuStorePayOrderId>(GetTransientPackage());
        purchase->orderId->value = field->GetStringField("value");
    }
    purchase->price->value = jsonPurchase->GetObjectField("price")->GetIntegerField("value");
    purchase->productId->value = jsonPurchase->GetObjectField("productId")->GetStringField("value");
    purchase->productType = GetEnumByNameString<EURuStorePayProductType>(jsonPurchase->GetStringField("productType"));
    purchase->purchaseId->value = jsonPurchase->GetObjectField("purchaseId")->GetStringField("value");
    if (auto field = jsonPurchase->HasField("purchaseTime"))
    {
        purchase->purchaseTime = NewObject<URuStorePayDate>(GetTransientPackage());
        purchase->purchaseTime->value = jsonPurchase->GetStringField("purchaseTime");
    }
    purchase->purchaseType = GetEnumByNameString<EURuStorePayPurchaseType>(jsonPurchase->GetStringField("purchaseType"));
    purchase->quantity->value = jsonPurchase->GetObjectField("quantity")->GetIntegerField("value");
    purchase->status = GetEnumByNameString<EURuStorePayPurchaseStatus>(jsonPurchase->GetStringField("status"));
    if (auto field = jsonPurchase->GetObjectField("subscriptionToken"))
    {
        purchase->subscriptionToken = NewObject<URuStorePaySubscriptionToken>(GetTransientPackage());
        purchase->subscriptionToken->value = field->GetStringField("value");
    }

    return purchase;
}

const TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe> DataConverter::ParseProductPurchase(const FString& json)
{
    TSharedPtr<FJsonObject> jsonResult;
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(json);
    if (!FJsonSerializer::Deserialize(reader, jsonResult) || !jsonResult.IsValid()) return nullptr;

    auto type = jsonResult->GetStringField("type");
    auto data = jsonResult->GetObjectField("data");

    if (type.Contains("SuccessProductPurchaseResult"))
    {
        const auto success = MakeShared<FURuStorePaySuccessProductPurchaseResult, ESPMode::ThreadSafe>();

        success->invoiceId->value = data->GetObjectField("invoiceId")->GetStringField("value");
        if (auto field = data->GetObjectField("orderId"))
        {
            success->orderId = NewObject<URuStorePayOrderId>(GetTransientPackage());
            success->orderId->value = field->GetStringField("value");
        }
        success->productId->value = data->GetObjectField("productId")->GetStringField("value");
        success->purchaseId->value = data->GetObjectField("purchaseId")->GetStringField("value");
        if (auto field = data->GetObjectField("subscriptionToken"))
        {
            success->subscriptionToken = NewObject<URuStorePaySubscriptionToken>(GetTransientPackage());
            success->subscriptionToken->value = field->GetStringField("value");
        }

        return success;
    }
    else
        if (type.Contains("CancelProductPurchaseResult"))
        {
            const auto cancel = MakeShared<FURuStorePayCancelledProductPurchaseResult, ESPMode::ThreadSafe>();

            if (auto field = data->GetObjectField("purchaseId"))
            {
                cancel->purchaseId = NewObject<URuStorePayPurchaseId>(GetTransientPackage());
                cancel->purchaseId->value = field->GetStringField("value");
            }

            return cancel;
        }
        else
            if (type.Contains("FailureProductPurchaseResult"))
            {
                const auto failure = MakeShared<FURuStorePayFailureProductPurchaseResult, ESPMode::ThreadSafe>();

                auto cause = data->GetObjectField("cause");
                failure->cause.name = data->GetStringField("simpleName");
                failure->cause.description = data->GetStringField("detailMessage");

                if (auto field = data->GetObjectField("invoiceId"))
                {
                    failure->invoiceId = NewObject<URuStorePayInvoiceId>(GetTransientPackage());
                    failure->invoiceId->value = field->GetStringField("value");
                }

                if (auto field = data->GetObjectField("orderId"))
                {
                    failure->orderId = NewObject<URuStorePayOrderId>(GetTransientPackage());
                    failure->orderId->value = field->GetStringField("value");
                }

                if (auto field = data->GetObjectField("productId"))
                {
                    failure->productId = NewObject<URuStorePayProductId>(GetTransientPackage());
                    failure->productId->value = field->GetStringField("value");
                }

                if (auto field = data->GetObjectField("purchaseId"))
                {
                    failure->purchaseId = NewObject<URuStorePayPurchaseId>(GetTransientPackage());
                    failure->purchaseId->value = field->GetStringField("value");
                }

                if (auto field = data->GetObjectField("quantity"))
                {
                    failure->quantity = NewObject<URuStorePayQuantity>(GetTransientPackage());
                    failure->quantity->value = field->GetIntegerField("value");
                }

                if (auto field = data->GetObjectField("subscriptionToken"))
                {
                    failure->subscriptionToken = NewObject<URuStorePaySubscriptionToken>(GetTransientPackage());
                    failure->subscriptionToken->value = field->GetStringField("value");
                }

                return failure;
            }

    return nullptr;
}
