// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FURuStoreError.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "URuStorePayInvoiceId.h"
#include "URuStorePayOrderId.h"
#include "URuStorePayPurchaseId.h"
#include "URuStorePayProductId.h"
#include "URuStorePayQuantity.h"
#include "URuStorePaySubscriptionToken.h"

#include "FURuStorePayFailureProductPurchaseResult.generated.h"

/*!
@brief При отправке запроса на оплату или получения статуса оплаты возникла проблема, невозможно установить статус покупки.
*/
USTRUCT(BlueprintType)
struct RUSTOREPAY_API FURuStorePayFailureProductPurchaseResult : public FURuStorePayProductPurchaseResult
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Информация об ошибке.
    */
    UPROPERTY(BlueprintReadOnly)
    FURuStoreError cause;

    /*!
    @brief Идентификатор счёта (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayInvoiceId* invoiceId; // nullable

    /*!
    @brief
        Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
        Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
        Если не укажете, он будет сгенерирован автоматически (uuid).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayOrderId* orderId; // nullable

    /*!
    @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayProductId* productId; // nullable

    /*!
    @brief Идентификатор покупки (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPurchaseId* purchaseId; // nullable

    /*!
    @brief Количество продукта (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayQuantity* quantity; // nullable
    
    /*!
    @brief Токен для валидации покупки на сервере (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePaySubscriptionToken* subscriptionToken; // nullable

    /*!
    @brief Получить имя типа.
    */
    virtual FString GetTypeName() override { return "FURuStorePayFailureProductPurchaseResult"; }

    /*!
    @brief Конструктор.
    */
    FURuStorePayFailureProductPurchaseResult()
    {
        invoiceId = nullptr;
        orderId = nullptr;
        productId = nullptr;
        purchaseId = nullptr;
        quantity = nullptr;
        subscriptionToken = nullptr;
    }
};
