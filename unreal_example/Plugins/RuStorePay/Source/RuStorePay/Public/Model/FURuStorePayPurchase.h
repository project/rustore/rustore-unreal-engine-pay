// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStorePayPurchaseType.h"
#include "EURuStorePayPurchaseStatus.h"
#include "URuStorePayPurchaseId.h"
#include "URuStorePayProductId.h"
#include "URuStorePayInvoiceId.h"
#include "URuStorePayOrderId.h"
#include "URuStorePayDescription.h"
#include "URuStorePayDate.h"
#include "URuStorePayPrice.h"
#include "URuStorePayAmountLabel.h"
#include "URuStorePayCurrency.h"
#include "URuStorePayQuantity.h"
#include "URuStorePaySubscriptionToken.h"
#include "URuStorePayDeveloperPayload.h"

#include "FURuStorePayPurchase.generated.h"

/*!
@brief Информация о покупке.
*/
USTRUCT(BlueprintType)
struct FURuStorePayPurchase
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Отформатированная цена покупки, включая валютный знак.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayAmountLabel* amountLabel;

    /*!
    @brief Код валюты ISO 4217.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayCurrency* currency;

    /*!
    @brief Описание на языке language (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayDescription* description;

    /*!
    @brief Строка с дополнительной информацией о заказе, которую вы можете установить при инициализации процесса покупки (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayDeveloperPayload* developerPayload; // nullable

    /*!
    @brief Идентификатор счёта.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayInvoiceId* invoiceId;

    /*!
    @brief
        Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
        Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
        Если не укажете, он будет сгенерирован автоматически (uuid).
        Максимальная длина 150 символов.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayOrderId* orderId; // nullable

    /*!
    @brief Цена в минимальных единицах (например в копейках).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPrice* price;

    /*!
    @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayProductId* productId;

    /*!
    @brief Тип продукта.
    */
    UPROPERTY(BlueprintReadOnly)
    EURuStorePayProductType productType;

    /*!
    @brief Идентификатор покупки.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPurchaseId* purchaseId;

    /*!
    @brief Время покупки (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayDate* purchaseTime; // nullable

    /*!
    @brief Тип покупки.
    */
    UPROPERTY(BlueprintReadOnly)
    EURuStorePayPurchaseType purchaseType;

    /*!
    @brief Количество продукта.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayQuantity* quantity;

    /*!
    @brief Статус покупки.
    */
    UPROPERTY(BlueprintReadOnly)
    EURuStorePayPurchaseStatus status;

    /*!
    @brief Токен для валидации покупки на сервере (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePaySubscriptionToken* subscriptionToken; // nullable

    /*!
    @brief Конструктор.
    */
    FURuStorePayPurchase()
    {
        amountLabel = NewObject<URuStorePayAmountLabel>(GetTransientPackage());
        currency = NewObject<URuStorePayCurrency>(GetTransientPackage());
        description = NewObject<URuStorePayDescription>(GetTransientPackage());
        developerPayload = nullptr;
        invoiceId = NewObject<URuStorePayInvoiceId>(GetTransientPackage());
        orderId = nullptr;
        price = NewObject<URuStorePayPrice>();
        productId = NewObject<URuStorePayProductId>(GetTransientPackage());
        productType = static_cast<EURuStorePayProductType>(0);
        purchaseId = NewObject<URuStorePayPurchaseId>(GetTransientPackage());
        purchaseTime = nullptr;
        purchaseType = static_cast<EURuStorePayPurchaseType>(0);
        quantity = NewObject<URuStorePayQuantity>(GetTransientPackage());
        status = static_cast<EURuStorePayPurchaseStatus>(0);
        subscriptionToken = nullptr;
    }
};
