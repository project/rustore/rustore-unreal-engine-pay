// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayTitle.generated.h"

/*!
@brief Название продукта.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayTitle : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
