// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "URuStorePayPurchaseId.h"

#include "FURuStorePayCancelledProductPurchaseResult.generated.h"

/*!
@brief Запрос на покупку отправлен, при этом пользователь закрыл «платёжную шторку» на своём устройстве, и результат оплаты неизвестен.
*/
USTRUCT(BlueprintType)
struct RUSTOREPAY_API FURuStorePayCancelledProductPurchaseResult : public FURuStorePayProductPurchaseResult
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Идентификатор покупки.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPurchaseId* purchaseId; // nullable

    /*!
    @brief Получить имя типа.
    */
    virtual FString GetTypeName() override { return "FURuStorePayCancelledProductPurchaseResult"; }

    /*!
    @brief Конструктор.
    */
    FURuStorePayCancelledProductPurchaseResult()
    {
        purchaseId = nullptr;
    }
};
