// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayQuantity.generated.h"

/*!
@brief Количество продукта.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayQuantity : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    int value;
};
