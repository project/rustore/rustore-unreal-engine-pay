// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FURuStorePayProductPurchaseResult.generated.h"

/*!
@brief Родительский класс результатов покупки.
*/
USTRUCT(BlueprintType)
struct RUSTOREPAY_API FURuStorePayProductPurchaseResult
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Деструктор.
    */
    virtual ~FURuStorePayProductPurchaseResult() {}

    /*!
    @brief Получить имя типа.
    */
    virtual FString GetTypeName() { return "FURuStorePayProductPurchaseResult"; }
};
