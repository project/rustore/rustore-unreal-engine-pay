// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStorePayPurchaseStatus.generated.h"

/*!
@brief Статус покупки.
*/
UENUM(BlueprintType)
enum class EURuStorePayPurchaseStatus : uint8
{
    /*!
    @brief Создан счет на оплату, покупка ожидает оплаты.
    */
    INVOICE_CREATED UMETA(DisplayName = "INVOICE_CREATED"),

    /*!
    @brief Покупка отменена покупателем.
    */
    CANCELLED UMETA(DisplayName = "CANCELLED"),
    
    /*!
    @brief Запущена оплата.
    */
    PROCESSING UMETA(DisplayName = "PROCESSING"),
    
    /*!
    @brief Покупка отклонена — недостаточно средств, карта заблокирована и т.д.
    */
    REJECTED UMETA(DisplayName = "REJECTED"),
    
    /*!
    @brief Покупка успешно оплачена.
    */
    CONFIRMED UMETA(DisplayName = "CONFIRMED"),
    
    CONSUMED UMETA(DisplayName = "CONSUMED"),
    
    /*!
    @brief Запрос на возврат средств за покупку совершен совершен успешно.
    */
    REFUNDED UMETA(DisplayName = "REFUNDED"),
    
    EXECUTING UMETA(DisplayName = "EXECUTING"),
    EXPIRED UMETA(DisplayName = "EXPIRED"),
    
    /*!
    @brief
        Только для двухстадийной оплаты.
        Холдирование средств на карте пользователя прошло успешно, покупка ожидает подтверждения.
    */
    PAID UMETA(DisplayName = "PAID"),
    
    /*!
    @brief
        Только для двухстадийной оплаты.
        Покупка была отменена разработчиком или не было произведено подтверждение покупки в течение 72 часов.
        Холдирование средств отменено.
    */
    REVERSED UMETA(DisplayName = "REVERSED")
};
