// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStorePayPurchaseType.generated.h"

/*!
@brief Тип покупки.
*/
UENUM(BlueprintType)
enum class EURuStorePayPurchaseType : uint8
{
    /*!
    @brief Одностадийная оплата.
    */
    ONE_STEP UMETA(DisplayName = "ONE_STEP"),
    
    /*!
    @brief Двухстадийная оплата.
    */
    TWO_STEP UMETA(DisplayName = "TWO_STEP")
};
