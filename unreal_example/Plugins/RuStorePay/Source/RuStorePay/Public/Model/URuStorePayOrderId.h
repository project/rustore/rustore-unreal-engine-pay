// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayOrderId.generated.h"

/*!
@brief Уникальный идентификатор оплаты, указанный разработчиком или сформированный автоматически (uuid).
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayOrderId : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
