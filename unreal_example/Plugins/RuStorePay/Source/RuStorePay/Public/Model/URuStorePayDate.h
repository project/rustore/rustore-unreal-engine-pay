// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayDate.generated.h"

/*!
@brief Дата и время покупки
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayDate : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
