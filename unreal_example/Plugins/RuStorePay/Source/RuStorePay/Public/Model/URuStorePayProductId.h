// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayProductId.generated.h"

/*!
@brief Идентификатор продукта, указанный при создании продукта в консоли разработчика.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayProductId : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
