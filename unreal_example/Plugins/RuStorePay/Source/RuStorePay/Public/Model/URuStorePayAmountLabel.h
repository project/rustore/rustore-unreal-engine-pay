// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayAmountLabel.generated.h"

/*!
@brief Отформатированная цена товара, включая валютный знак.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayAmountLabel : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
