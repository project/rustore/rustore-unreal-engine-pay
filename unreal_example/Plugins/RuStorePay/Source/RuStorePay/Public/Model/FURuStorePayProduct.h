// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStorePayProductType.h"
#include "URuStorePayAmountLabel.h"
#include "URuStorePayCurrency.h"
#include "URuStorePayDescription.h"
#include "URuStorePayPrice.h"
#include "URuStorePayProductId.h"
#include "URuStorePayTitle.h"
#include "URuStorePayUrl.h"

#include "FURuStorePayProduct.generated.h"

/*!
@brief Информация о продукте.
*/
USTRUCT(BlueprintType)
struct FURuStorePayProduct
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Отформатированная цена покупки, включая валютный знак.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayAmountLabel* amountLabel;

    /*!
    @brief Код валюты ISO 4217.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayCurrency* currency;

    /*!
    @brief Описание на языке language (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayDescription* description; // nullable

    /*!
    @brief Ссылка на картинку.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayUrl* imageUrl;

    /*!
    @brief Цена в минимальных единицах (например в копейках) (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPrice* price; // nullable

    /*!
    @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayProductId* productId;

    UPROPERTY(BlueprintReadOnly)
    URuStorePayUrl* promoImageUrl; // nullable

    /*!
    @brief Название продукта на языке language.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayTitle* title;

    /*!
    @brief Тип продукта.
    */
    UPROPERTY(BlueprintReadOnly)
    EURuStorePayProductType type;

    /*!
    @brief Конструктор.
    */
    FURuStorePayProduct()
    {
        amountLabel = NewObject<URuStorePayAmountLabel>();
        currency = NewObject<URuStorePayCurrency>();
        description = nullptr;
        imageUrl = NewObject<URuStorePayUrl>();
        price = nullptr;
        promoImageUrl = nullptr;
        productId = NewObject<URuStorePayProductId>();
        title = NewObject<URuStorePayTitle>();
        type = static_cast<EURuStorePayProductType>(0);
    }
};
