// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStorePayProductType.generated.h"

/*!
@brief Тип продукта.
*/
UENUM(BlueprintType)
enum class EURuStorePayProductType : uint8
{
    APPLICATION UMETA(DisplayName = "APPLICATION"),
    
    /*!
    @brief
        Непотребляемй товар.
        Можно купить один раз.
    */
    NON_CONSUMABLE_PRODUCT UMETA(DisplayName = "NON_CONSUMABLE_PRODUCT"),
    
    /*!
    @brief
        Потребляемый товар.
        Можно купить много раз.
    */
    CONSUMABLE_PRODUCT UMETA(DisplayName = "CONSUMABLE_PRODUCT")
};
