// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayCurrency.generated.h"

/*!
@brief Код валюты ISO 4217.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayCurrency : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
