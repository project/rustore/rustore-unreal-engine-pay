// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayPrice.generated.h"

/*!
@brief Цена в минимальных единицах валюты.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayPrice : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    int value;
};
