// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayDescription.generated.h"

/*!
@brief Описание продукта/покупки.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayDescription : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
