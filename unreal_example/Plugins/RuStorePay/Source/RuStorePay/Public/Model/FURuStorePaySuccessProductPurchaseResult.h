// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "URuStorePayInvoiceId.h"
#include "URuStorePayOrderId.h"
#include "URuStorePayPurchaseId.h"
#include "URuStorePayProductId.h"
#include "URuStorePaySubscriptionToken.h"

#include "FURuStorePaySuccessProductPurchaseResult.generated.h"

/*!
@brief Результат успешного завершения покупки цифрового товара.
*/
USTRUCT(BlueprintType)
struct RUSTOREPAY_API FURuStorePaySuccessProductPurchaseResult : public FURuStorePayProductPurchaseResult
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Идентификатор счёта.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayInvoiceId* invoiceId;

    /*!
    @brief
        Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
        Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
        Если не укажете, он будет сгенерирован автоматически (uuid).
        Максимальная длина 150 символов.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayOrderId* orderId; // nullable

    /*!
    @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayProductId* productId;

    /*!
    @brief Идентификатор покупки.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayPurchaseId* purchaseId;

    /*!
    @brief Токен для валидации покупки на сервере (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePaySubscriptionToken* subscriptionToken; // nullable

    /*!
    @brief Получить имя типа.
    */
    virtual FString GetTypeName() override { return "FURuStorePaySuccessProductPurchaseResult"; }

    /*!
    @brief Конструктор.
    */
    FURuStorePaySuccessProductPurchaseResult()
    {
        invoiceId = NewObject<URuStorePayInvoiceId>(GetTransientPackage());
        orderId = nullptr;
        productId = NewObject<URuStorePayProductId>(GetTransientPackage());
        purchaseId = NewObject<URuStorePayPurchaseId>(GetTransientPackage());
        subscriptionToken = nullptr;
    }
};
