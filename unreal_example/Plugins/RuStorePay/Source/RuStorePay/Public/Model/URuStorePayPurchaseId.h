// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayPurchaseId.generated.h"

/*!
@brief Идентификатор покупки.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayPurchaseId : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
