// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "URuStorePayDeveloperPayload.h"
#include "URuStorePayOrderId.h"
#include "URuStorePayProductId.h"
#include "URuStorePayQuantity.h"

#include "FURuStorePayProductPurchaseParams.generated.h"

/*!
@brief Параметры покупки продукта.
*/
USTRUCT(BlueprintType)
struct FURuStorePayProductPurchaseParams
{
    GENERATED_USTRUCT_BODY()

    /*!
    @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayProductId* productId;

    /*!
    @brief Строка с дополнительной информацией о заказе, которую вы можете установить при инициализации процесса покупки (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayDeveloperPayload* developerPayload; // nullable

    /*!
    @brief
        Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
        Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
        Если не укажете, он будет сгенерирован автоматически (uuid).
        Максимальная длина 150 символов.
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayOrderId* orderId; // nullable

    /*!
    @brief Количество продукта (необязательный параметр).
    */
    UPROPERTY(BlueprintReadOnly)
    URuStorePayQuantity* quantity; // nullable

    /*!
    @brief Конструктор.
    */
    FURuStorePayProductPurchaseParams()
    {
        productId = NewObject<URuStorePayProductId>();
        developerPayload = nullptr;
        orderId = nullptr;
        quantity = nullptr;
    }
};
