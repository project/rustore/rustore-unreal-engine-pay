// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayInvoiceId.generated.h"

/*!
@brief
    Идентификатор счета.
    Используется дл¤ серверной валидации платежа, поиска платежей в консоли разработчика,
    а также отображается покупателю в истории платежей в мобильном приложении RuStore.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayInvoiceId : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
