// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayUrl.generated.h"

/*!
@brief Веб-ссылка.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayUrl : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
