// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePaySubscriptionToken.generated.h"

/*!
@brief Токен для валидации покупки на сервере.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePaySubscriptionToken : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
