// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "URuStorePayDeveloperPayload.generated.h"

/*!
@brief Указанная разработчиком строка, содержащая дополнительную информацию о заказе.
*/
UCLASS(BlueprintType)
class RUSTOREPAY_API URuStorePayDeveloperPayload : public UObject
{
    GENERATED_BODY()

public:
    /*!
    @brief Значение.
    */
    UPROPERTY(BlueprintReadOnly)
    FString value;
};
