// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayGetPurchaseNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGetPurchasePin, FURuStorePayPurchase, response, FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayGetPurchaseNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FGetPurchasePin Success;

    UPROPERTY(BlueprintAssignable)
    FGetPurchasePin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayGetPurchaseNode* GetPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId);
};
