// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayCancelTwoStepPurchaseNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCancelTwoStepPurchasePin, FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayCancelTwoStepPurchaseNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FCancelTwoStepPurchasePin Success;

    UPROPERTY(BlueprintAssignable)
    FCancelTwoStepPurchasePin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayCancelTwoStepPurchaseNode* CancelTwoStepPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId);
};
