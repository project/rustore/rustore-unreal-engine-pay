// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayGetPurchaseAvailabilityNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGetPurchaseAvailabilityPin, FURuStorePayPurchaseAvailabilityResult, response, FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayGetPurchaseAvailabilityNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FGetPurchaseAvailabilityPin Success;

    UPROPERTY(BlueprintAssignable)
    FGetPurchaseAvailabilityPin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayGetPurchaseAvailabilityNode* GetPurchaseAvailabilityAsync(URuStorePayClient* target);
};
