// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayConfirmTwoStepPurchaseNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FConfirmTwoStepPurchasePin, FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayConfirmTwoStepPurchaseNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FConfirmTwoStepPurchasePin Success;

    UPROPERTY(BlueprintAssignable)
    FConfirmTwoStepPurchasePin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayConfirmTwoStepPurchaseNode* ConfirmTwoStepPurchaseAsync(URuStorePayClient* target, URuStorePayPurchaseId* purchaseId, URuStorePayDeveloperPayload* developerPayload);
};
