// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "FURuStorePayCancelledProductPurchaseResult.h"
#include "FURuStorePayFailureProductPurchaseResult.h"
#include "FURuStorePaySuccessProductPurchaseResult.h"
#include "FURuStorePayProductPurchaseParams.h"
#include "URuStorePayClient.h"

#include "URuStorePayPurchaseTwoStepNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FPurchaseTwoStepPin,
    FURuStorePaySuccessProductPurchaseResult, success,
    FURuStorePayCancelledProductPurchaseResult, cancelled,
    FURuStorePayFailureProductPurchaseResult, failure,
    FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayPurchaseTwoStepNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FPurchaseTwoStepPin Success;
    
    UPROPERTY(BlueprintAssignable)
    FPurchaseTwoStepPin Cancelled;

    UPROPERTY(BlueprintAssignable)
    FPurchaseTwoStepPin Failure;

    UPROPERTY(BlueprintAssignable)
    FPurchaseTwoStepPin Error;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayPurchaseTwoStepNode* PurchaseTwoStepAsync(URuStorePayClient* target, FURuStorePayProductPurchaseParams productPurchaseParams);
};
