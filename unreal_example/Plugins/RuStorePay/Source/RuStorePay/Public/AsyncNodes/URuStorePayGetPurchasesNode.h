// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayGetPurchasesNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGetPayPurchasesPin, const TArray<FURuStorePayPurchase>&, response, FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayGetPurchasesNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FGetPayPurchasesPin Success;

    UPROPERTY(BlueprintAssignable)
    FGetPayPurchasesPin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayGetPurchasesNode* GetPurchasesAsync(URuStorePayClient* target);
};
