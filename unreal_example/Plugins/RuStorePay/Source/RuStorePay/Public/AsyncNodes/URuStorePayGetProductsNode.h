// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStorePayClient.h"
#include "URuStorePayProductId.h"

#include "URuStorePayGetProductsNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGetPayProductsPin, const TArray<FURuStorePayProduct>&, response, FURuStoreError, error);

UCLASS()
    class RUSTOREPAY_API URuStorePayGetProductsNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FGetPayProductsPin Success;

    UPROPERTY(BlueprintAssignable)
    FGetPayProductsPin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayGetProductsNode* GetProductsAsync(URuStorePayClient* target, TArray<URuStorePayProductId*> productIds);
};
