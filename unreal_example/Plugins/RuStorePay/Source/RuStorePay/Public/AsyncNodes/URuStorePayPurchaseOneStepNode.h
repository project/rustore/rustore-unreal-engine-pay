// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "FURuStorePayCancelledProductPurchaseResult.h"
#include "FURuStorePayFailureProductPurchaseResult.h"
#include "FURuStorePaySuccessProductPurchaseResult.h"
#include "FURuStorePayProductPurchaseParams.h"
#include "URuStorePayClient.h"

#include "URuStorePayPurchaseOneStepNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FPurchaseOneStepPin,
    FURuStorePaySuccessProductPurchaseResult, success,
    FURuStorePayCancelledProductPurchaseResult, cancelled,
    FURuStorePayFailureProductPurchaseResult, failure,
    FURuStoreError, error);

UCLASS()
class RUSTOREPAY_API URuStorePayPurchaseOneStepNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FPurchaseOneStepPin Success;
    
    UPROPERTY(BlueprintAssignable)
    FPurchaseOneStepPin Cancelled;

    UPROPERTY(BlueprintAssignable)
    FPurchaseOneStepPin Failure;

    UPROPERTY(BlueprintAssignable)
    FPurchaseOneStepPin Error;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore Pay Client")
    static URuStorePayPurchaseOneStepNode* PurchaseOneStepAsync(URuStorePayClient* target, FURuStorePayProductPurchaseParams productPurchaseParams);
};
