// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "AndroidJavaObject.h"
#include "CoreMinimal.h"
#include "FURuStorePayProduct.h"
#include "FURuStorePayPurchaseAvailabilityResult.h"
#include "FURuStorePayPurchase.h"
#include "FURuStoreError.h"
#include "FURuStorePayProductPurchaseParams.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "RuStoreListener.h"
#include "UObject/Object.h"
#include "URuStorePayDeveloperPayload.h"
#include "URuStorePayOrderId.h"
#include "URuStorePayProductId.h"
#include "URuStorePayPurchaseId.h"
#include "URuStorePayQuantity.h"

#include "URuStorePayClient.generated.h"

using namespace RuStoreSDK;

/*!
@brief Класс реализует API для интеграции платежей в мобильное приложение.
*/
UCLASS(Blueprintable)
class RUSTOREPAY_API URuStorePayClient : public UObject, public RuStoreListenerContainer
{
	GENERATED_BODY()

private:
    static URuStorePayClient* _instance;
    static bool _bIsInstanceInitialized;

    bool bIsInitialized = false;
    bool _bAllowNativeErrorHandling = false;
    AndroidJavaObject* _clientWrapper = nullptr;

public:
    /*!
    @brief Версия плагина.
    */
    static const FString PluginVersion;

    /*!
    @brief Проверка инициализации менеджера.
    @return Возвращает true, если синглтон инициализирован, в противном случае — false.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    bool GetIsInitialized();

    /*!
    @brief
       Получить экземпляр URuStorePayClient.
    @return
       Возвращает указатель на единственный экземпляр URuStorePayClient (реализация паттерна Singleton).
       Если экземпляр еще не создан, создает его.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static URuStorePayClient* Instance();

    /*!
    @brief Выполняет инициализацию синглтона URuStorePayClient.
    @return Возвращает true, если инициализация была успешно выполнена, в противном случае — false.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    bool Init();

    /*!
    @brief Деинициализация синглтона, если дальнейшая работа с объектом больше не планируется.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    void Dispose();

    void ConditionalBeginDestroy();

    /*!
    @brief
        Проверка доступности платежей.
        Если все условия выполняются, возвращается FURuStorePayPurchaseAvailabilityResult::isAvailable == true.
        В противном случае возвращается FURuStorePayPurchaseAvailabilityResult::isAvailable == false.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и объект FURuStorePayPurchaseAvailabilityResult с информцаией о доступности оплаты.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long GetPurchaseAvailability(TFunction<void(long, TSharedPtr<FURuStorePayPurchaseAvailabilityResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief Получение списка продуктов, добавленных в ваше приложение через консоль RuStore.
    @param productIds
        Список идентификаторов продуктов (задаются при создании продукта в консоли разработчика).
        Список продуктов имеет ограничение в размере 1000 элементов.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и список объектов FURuStorePayProduct с информцаией о продуктах.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long GetProducts(TArray<URuStorePayProductId*>& productIds, TFunction<void(long, TSharedPtr<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief Получение списка покупок пользователя.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и список объектов FURuStorePayPurchase с информцаией о покупках.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long GetPurchases(TFunction<void(long, TSharedPtr<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief Получение информации о покупке.
    @param purchaseId Идентификатор продукта, который был присвоен продукту в RuStore Консоли.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и объект FURuStorePayPurchase с информцаией о покупке.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long GetPurchase(URuStorePayPurchaseId* purchaseId, TFunction<void(long, TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief Покупки продукта с одностадийной оплатой.
    @param productPurchaseParams Параметры покупки продукта.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и объект реализующий интерфейс FURuStorePayProductPurchaseResult с информцаией о результате покупки.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long PurchaseOneStep(FURuStorePayProductPurchaseParams& productPurchaseParams, TFunction<void(long, TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief Покупки продукта с двустадийной оплатой.
    @param productPurchaseParams Параметры покупки продукта.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и объект реализующий интерфейс FURuStorePayProductPurchaseResult с информцаией о результате покупки.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long PurchaseTwoStep(FURuStorePayProductPurchaseParams& productPurchaseParams, TFunction<void(long, TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief
        Потребление (подтверждение) покупки.
        После вызова подтверждения, покупка перейдёт в статус CONFIRMED.
        Запрос на потребление (подтверждение) покупки должен сопровождаться выдачей товара.
    @param purchaseId Идентификатор покупки.
    @param developerPayload Строка, содержащая дополнительную информацию о заказе (необязательный параметр).
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long ConfirmTwoStepPurchase(URuStorePayPurchaseId* purchaseId, URuStorePayDeveloperPayload* developerPayload, TFunction<void(long)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);
    
    /*!
    @brief
        Отмена покупки.
        Запрос на потребление (подтверждение) покупки должен сопровождаться выдачей товара.
    @param purchaseId Идентификатор покупки.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long CancelTwoStepPurchase(URuStorePayPurchaseId* purchaseId, TFunction<void(long)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);

    /*!
    @brief Проверка установлен ли на устройстве пользователя RuStore.
    @return Возвращает true, если RuStore установлен, в противном случае — false.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    bool IsRuStoreInstalled();

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static URuStorePayProductId* MakeProductId(FString productId);

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static TArray<URuStorePayProductId*> MakeProductIdArray(TArray<FString> productIds);

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static URuStorePayQuantity* MakeQuantity(int value = 1);

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static URuStorePayOrderId* MakeOrderId(FString value);

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static URuStorePayDeveloperPayload* MakeDeveloperPayload(FString value);

    UFUNCTION(BlueprintCallable, Category = "RuStore Pay Client")
    static FURuStorePayProductPurchaseParams MakeProductPurchaseParams(URuStorePayProductId* productId, URuStorePayDeveloperPayload* developerPayload, URuStorePayOrderId* orderId, URuStorePayQuantity* quantity);
};
