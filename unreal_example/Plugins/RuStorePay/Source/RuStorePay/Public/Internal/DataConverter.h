// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "AndroidJavaClass.h"
#include "CoreMinimal.h"
#include "FURuStorePayProduct.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "FURuStorePayPurchase.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class DataConverter
        {
        public:
            static const TSharedRef<TArray<FURuStorePayProduct>, ESPMode::ThreadSafe> ParseProducts(const FString& json);
            static const TSharedRef<TArray<FURuStorePayPurchase>, ESPMode::ThreadSafe> ParsePurchases(const FString& json);
            static const TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe> ParsePurchase(const FString& json);
            static const TSharedPtr<FURuStorePayPurchase, ESPMode::ThreadSafe> ParsePurchase(TSharedPtr<FJsonObject> jsonPurchase);
            static const TSharedPtr<FURuStorePayProductPurchaseResult, ESPMode::ThreadSafe> ParseProductPurchase(const FString& json);

            template <typename T>
            static T GetEnumByNameString(const FString& name) {
                const UEnum* enumPtr = StaticEnum<T>();
                const uint8 index = enumPtr->GetIndexByNameString(name);
                return static_cast<T>(index);
            }
        };
    }
}
