// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SimpleResponseListenerT.h"
#include "FURuStorePayProductPurchaseResult.h"
#include "DataConverter.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API ProductPurchaseResultListenerImpl : public SimpleResponseListenerT<FString>
        {
        public:
            ProductPurchaseResultListenerImpl(
                TFunction<void(long, FString)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListenerT<FString>("ru/rustore/unrealsdk/payclient/wrappers/ProductPurchaseResultListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/ProductPurchaseResultListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
