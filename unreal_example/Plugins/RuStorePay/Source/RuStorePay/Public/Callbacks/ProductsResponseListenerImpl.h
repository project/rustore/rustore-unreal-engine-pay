// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SimpleResponseListenerT.h"
#include "FURuStorePayProduct.h"
#include "DataConverter.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API ProductsResponseListenerImpl : public SimpleResponseListenerT<FString>
        {
        public:
            ProductsResponseListenerImpl(
                TFunction<void(long, FString)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListenerT<FString>("ru/rustore/unrealsdk/payclient/wrappers/ProductsResponseListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/ProductsResponseListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
