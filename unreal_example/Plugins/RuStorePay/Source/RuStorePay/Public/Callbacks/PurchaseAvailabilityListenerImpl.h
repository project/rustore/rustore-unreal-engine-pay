// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "URuStoreCore.h"
#include "FURuStorePayPurchaseAvailabilityResult.h"
#include "ResponseListener.h"

namespace RuStoreSDK
{
	namespace Pay
	{
		class RUSTOREPAY_API PurchaseAvailabilityListenerImpl : public ResponseListener<FURuStorePayPurchaseAvailabilityResult>
		{
		protected:
			FURuStorePayPurchaseAvailabilityResult* ConvertResponse(AndroidJavaObject* responseObject) override;

		public:
			PurchaseAvailabilityListenerImpl(
				TFunction<void(long requestId, TSharedPtr<FURuStorePayPurchaseAvailabilityResult, ESPMode::ThreadSafe>)> onSuccess,
				TFunction<void(long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
				TFunction<void(RuStoreListener*)> onFinish
			) : ResponseListener<FURuStorePayPurchaseAvailabilityResult>("ru/rustore/unrealsdk/payclient/wrappers/PurchaseAvailabilityListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/PurchaseAvailabilityListener", onSuccess, onFailure, onFinish)
			{
			}

			virtual ~PurchaseAvailabilityListenerImpl();
		};
	}
}
