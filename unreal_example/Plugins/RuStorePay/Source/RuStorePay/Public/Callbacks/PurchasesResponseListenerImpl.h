// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SimpleResponseListenerT.h"
#include "FURuStorePayPurchase.h"
#include "DataConverter.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API PurchasesResponseListenerImpl : public SimpleResponseListenerT<FString>
        {
        public:
            PurchasesResponseListenerImpl(
                TFunction<void(long, FString)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListenerT<FString>("ru/rustore/unrealsdk/payclient/wrappers/PurchasesResponseListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/PurchasesResponseListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
