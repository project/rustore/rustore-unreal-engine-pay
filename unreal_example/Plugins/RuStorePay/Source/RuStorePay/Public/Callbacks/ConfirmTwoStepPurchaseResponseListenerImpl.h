// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "FURuStoreError.h"
#include "SimpleResponseListener.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API ConfirmTwoStepPurchaseResponseListenerImpl : public SimpleResponseListener
        {
        public:
            ConfirmTwoStepPurchaseResponseListenerImpl(
                TFunction<void(long)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListener("ru/rustore/unrealsdk/payclient/wrappers/ConfirmTwoStepPurchaseResponseListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/ConfirmTwoStepPurchaseListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
