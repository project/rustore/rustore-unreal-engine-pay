// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SimpleResponseListenerT.h"
#include "FURuStorePayPurchase.h"
#include "DataConverter.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API PurchaseResponseListenerImpl : public SimpleResponseListenerT<FString>
        {
        public:
            PurchaseResponseListenerImpl(
                TFunction<void(long, FString)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListenerT<FString>("ru/rustore/unrealsdk/payclient/wrappers/PurchaseResponseListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/PurchaseResponseListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
