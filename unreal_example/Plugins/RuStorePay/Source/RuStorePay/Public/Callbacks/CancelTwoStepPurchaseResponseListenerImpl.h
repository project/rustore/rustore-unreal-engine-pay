// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "FURuStoreError.h"
#include "SimpleResponseListener.h"

namespace RuStoreSDK
{
    namespace Pay
    {
        class RUSTOREPAY_API CancelTwoStepPurchaseResponseListenerImpl : public SimpleResponseListener
        {
        public:
            CancelTwoStepPurchaseResponseListenerImpl(
                TFunction<void(long)> onSuccess,
                TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
                TFunction<void(RuStoreListener*)> onFinish
            ) : SimpleResponseListener("ru/rustore/unrealsdk/payclient/wrappers/CancelTwoStepPurchaseResponseListenerWrapper", "ru/rustore/unrealsdk/payclient/callbacks/CancelTwoStepPurchaseListener", onSuccess, onFailure, onFinish)
            {
            }
        };
    }
}
