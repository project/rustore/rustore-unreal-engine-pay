// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class RuStorePay : ModuleRules
{
	public RuStorePay(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		string publicPath = Path.Combine(ModuleDirectory, "Public");
		string privatePath = Path.Combine(ModuleDirectory, "Private");

		PublicIncludePaths.AddRange(
			new string[]
			{
				publicPath,
				Path.Combine(publicPath, "AsyncNodes"),
				Path.Combine(publicPath, "Callbacks"),
				Path.Combine(publicPath, "Model"),
				Path.Combine(publicPath, "Internal"),
			}
		);

		PrivateIncludePaths.AddRange(
			new string[]
			{
				privatePath,
				Path.Combine(privatePath, "AsyncNodes"),
				Path.Combine(privatePath, "Callbacks"),
				Path.Combine(privatePath, "Internal"),
			}
		);

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"RuStoreCore",
				"Json",
			}
		);
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
			}
		);
		
		if (Target.Platform == UnrealTargetPlatform.Android)
		{
			PrivateDependencyModuleNames.AddRange(new string[] { "Launch" });
			AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(ModuleDirectory, "RuStorePay_UPL_Android.xml"));
		}
	}
}
