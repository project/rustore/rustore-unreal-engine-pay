// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RuStorePayAppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RUSTOREPAYAPP_API ARuStorePayAppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
